# Ansible role to manage entropy tools

## Introduction

This role detects if a RNG hardware is present and install the RNG
tools to properly use it.

When no hardware is available, in order to avoid entropy depletion,
it falls back to using the software generator
[Haveged](http://www.issihosts.com/haveged/).

## Variables

none

